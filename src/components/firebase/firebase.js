import * as firebase from 'firebase'

export default firebase.initializeApp({
  apiKey: "AIzaSyAeDYNDYAfhY_Nc3NFMGOkh-SY3R7guowo",
  authDomain: "ico-kurs.firebaseapp.com",
  databaseURL: "https://ico-kurs.firebaseio.com",
  projectId: "ico-kurs",
  storageBucket: "ico-kurs.appspot.com",
  messagingSenderId: "252537420524"
})
