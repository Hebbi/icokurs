import React from 'react';
import News from './../news/news';
import Ico from './../ico/ico'

class Home extends React.Component {
    render(){
        return(
            <div>
                <div className='container'>
                    <div className='row'>
                        <section className='news-page col-xl-12'>
                            <News />
                        </section>
                        <hr />
                        
                        </div>
                        <section className='ico-page col-xl-12'>
                            <Ico />
                        </section>
                </div>
            </div>

        );
    }
}

export default Home;