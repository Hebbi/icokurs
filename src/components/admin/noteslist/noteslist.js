import React from "react";
import NoteItem from "./../notesitem/notesitem";
// import Masonry from "react-mason";

class NotesList extends React.Component {
//   componentDidMount = () => {
//     let grid = this.refs.grid;
//     this.msnry = new Masonry(grid, {
//       itemSelector: ".note",
//       columnWidth: 200,
//       gutter: 10
//     });
//   };

  render() {
    console.log(this.props);
    return (
      <div className="notesList">
        {this.props.Notes.map(note => {
          return (
            <NoteItem
            key={note.id}
            text={note.text}
            website={note.website}
            whitepaper={note.whitepaper}
            totalcoin={note.totalcoin}
            hardcap={note.hardcap}
            startdate={note.startdate}
            enddate={note.enddate}
            description={note.description}
            handleDeleteNotes={this.props.handleDeleteNotes.bind(null, note)}
            />
            
              
          );
        })}
      </div>
    );
  }
}

export default NotesList;
