import React from "react";
 
class NewsTextItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newstitle: "",
      newsimg: "",
      newsdescription: "",
      newsdate: "",  
      newserror: ""
    };
  }

  handleNewsTitleChange = event => {
    this.setState({
        newstitle: event.target.value,
    });
  };
  handleNewsImgChange = event => {
    this.setState({
        newsimg: event.target.value,
    });
  };
  handleNewsDescriptionChange = event => {
    this.setState({
        newsdescription: event.target.value,
    });
  };
  handleNewsDateChange = event => {
    this.setState({
        newsdate: event.target.value
    });
  };
  handleNewNewsAdd = () => {
    console.log("text", this.state.newstitle, this.state.newsimg, this.state.newsdescription, this.state.newsdate);

    if (this.state.newstitle, this.state.newsimg, this.state.newsdescription, this.state.newsdate){
      let newNews = {
        newsid: Date.now(),
        newstitle: this.state.newstitle,
        newsimg: this.state.newsimg,
        newsdescription: this.state.newsdescription,
        newsdate: this.state.newsdate,
      };
      this.props.newsAdd(newNews);
    }
    if (this.state.newstitle.length === 0) {
      this.setState({ error: "note is empty, please input something" });
    } 
    else if(this.state.newsimg.length === 0){
        this.setState({ error: "note is empty, please input something" });
    }
    else if(this.state.newsdescription.length === 0){
        this.setState({ error: "note is empty, please input something" });
    }
    else if(this.state.newsdate.length === 0){
        this.setState({ error: "note is empty, please input something" });
    }
    else {
      this.setState({ newserror: "" });
    }
  };

  render() {
    return (
      <div className="textAreaWrapper">
      <label>Enter title of news</label><br />
      <input type="text" onChange={this.handleNewsTitleChange} placeholder="name of news"/><br />
      <label>Enter img</label><br />
      <input type="file" name='mainimage'  onChange={this.handleNewsImgChange} /><br />
      <label>Enter description</label><br />
      <textarea type="text" onChange={this.handleNewsDescriptionChange} placeholder="description"> </ textarea><br />
      <label>Enter date</label><br />
      <input type="date" onChange={this.handleNewsDateChange} placeholder="date"/><br />

        <button className="btnAdd" onClick={this.handleNewNewsAdd}>
          Add
        </button>
        {this.state.newserror !== "" ? <div>{this.state.newserror}</div> : <div />}
      </div>
    );
  }
}

export default NewsTextItem;
