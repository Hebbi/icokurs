import React from "react";
 
class TextItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
      website: "",
      whitepaper: "",
      totalcoin: "",
      hardcap: "",
      startdate: "",
      enddate: "",
      description: "",  
      error: ""
    };
  }

  handleTextChange = event => {
    this.setState({
      text: event.target.value,
    });
  };
  handleWebsiteChange = event => {
    this.setState({
      website: event.target.value,
    });
  };
  handleWhitepaperChange = event => {
    this.setState({
      whitepaper: event.target.value,
    });
  };
  handleTotalcoinChange = event => {
    this.setState({
      totalcoin: event.target.value
    });
  };
  handleHardcapChange = event => {
    this.setState({
      hardcap: event.target.value,
    });
  };
  handleStartDateChange = event => {
    this.setState({
    startdate: event.target.value,
    });
  };
  handledEndatechange = event => {
    this.setState({
    enddate: event.target.value,
    });
  };
  handleDescriptionChange = event => {
    this.setState({
      description: event.target.value,
    });
  };
  handleNewNoteAdd = () => {
    console.log("text", this.state.text, this.state.website, this.state.whitepaper, this.state.totalcoin, this.state.hardcap, this.state.date, this.state.description);

    if (this.state.text, this.state.website, this.state.whitepaper, this.state.totalcoin, this.state.hardcap, this.state.date, this.state.description) {
      let newNote = {
        id: Date.now(),
        text: this.state.text,
        website: this.state.website,
        whitepaper: this.state.whitepaper,
        totalcoin: this.state.totalcoin,
        hardcap: this.state.hardcap,
        startdate: this.state.startdate,
        enddate: this.state.enddate,
        description: this.state.description
      };
      this.props.noteAdd(newNote);
    }
    if (this.state.text.length === 0) {
      this.setState({ error: "note is empty, please input something" });
    } 
    else if(this.state.website.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    else if(this.state.whitepaper.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    else if(this.state.totalcoin.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    else if(this.state.hardcap.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    else if(this.state.startdate.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    else if(this.state.enddate.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    
    else if(this.state.description.length === 0){
        this.setState({ error: "note is empty, please input something" });

    }
    

    else {
      this.setState({ error: "" });
    }
  };

  render() {
    return (
      <div className="textAreaWrapper">
      <label>Enter name of ICO</label><br />
      <input type="text" onChange={this.handleTextChange} placeholder="name of ico"/><br />
      <label>Enter website</label><br />
      <input type="text" onChange={this.handleWebsiteChange} placeholder="website"/><br />
      <label>Enter whitepaper</label><br />
      <input type="text" onChange={this.handleWhitepaperChange} placeholder="whitepaper"/><br />
      <label>Enter total coin</label><br />
      <input type="number" onChange={this.handleTotalcoinChange} placeholder="total coin"/><br />
      <label>Enter hard cap</label><br />
      <input type="number" onChange={this.handleHardcapChange} placeholder="hardcap"/><br />
      <label>Enter start date</label><br />
      <input type="date" onChange={this.handleStartDateChange} placeholder="start date"/><br />
      <label>Enter end date</label><br />
      <input type="date" onChange={this.handledEndatechange} placeholder="end date" /><br/>
      <label>Enter description</label><br />
      <input type="text" onChange={this.handleDescriptionChange} placeholder="description"/><br />

        {/* <textarea
          className="textarea"
          placeholder="Enter your note ..."
          rows={5}
          onChange={this.handleTextChange}
        /> */}
        <button className="btnAdd" onClick={this.handleNewNoteAdd}>
          Add
        </button>
        {this.state.error !== "" ? <div>{this.state.error}</div> : <div />}
      </div>
    );
  }
}

export default TextItem;
