import React from "react";
import "./notesitem.scss";

class NoteItem extends React.Component {
  render() {
    return (
      <div className="note">
        
        <div className="delete" onClick={this.props.handleDeleteNotes}>
          ×
        </div>
        <h4 className="ico-title">{this.props.text}</h4>
        website - <a href="{this.props.website}" className="ico-website">{this.props.website}</a><br />
        whitepaper - <a href="{this.props.whitepaper}" className="ico-whitepaper">{this.props.whitepaper}</a>
        <p className="ico-coin">Total coin: {this.props.totalcoin}</p>
        <p className="ico-hardcap">Hardcap: {this.props.hardcap}</p>
        <p className="ico-date">start date:{this.props.startdate} end date:{this.props.enddate}</p>
        <p className="ico-description"><b>description: {this.props.description}</b></p>
      </div>
    );
  }
}

export default NoteItem;
