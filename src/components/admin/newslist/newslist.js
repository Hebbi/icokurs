import React from "react";
import NewsItem from "./../newsitem/newsitem";

class NewsList extends React.Component {

  render() {
    console.log(this.props);
    return (
      <div className="newslist">
        {this.props.Newss.map(news => {
          return (
            <NewsItem
            newskey={news.newsid}
            newstitle={news.newstitle}
            newsimg={news.newsimg}
            newsdescription={news.newsdescription}
            newsdate={news.newsdate}
            handleDeleteNews={this.props.handleDeleteNews.bind(null, news)}
            />   
          );
        })}
      </div>
    );
  }
}

export default NewsList;
