import React from "react";
import "./newsitem.scss";

class NewsItem extends React.Component {
  render() {
    // let bgk = { background: this.props.color };
    return (
    <div className="newsitem">
        
        <div className="delete" onClick={this.props.handleDeleteNews}>
          ×
        </div>
        <h4 className="news-title">{this.props.newstitle}</h4>
        <img src='{this.props.newsimg}' alt='img' />
        <p className="news-description">description: {this.props.newsdescription}</p>
        <p className="news-date"><b>date: {this.props.newsdate}</b></p>
      </div>
    );
  }
}

export default NewsItem;
