import React from 'react';
import Addnews from './addnews/addnews';
import TextArea from './textitem/textitem';
import NotesList from './noteslist/noteslist';
import firebase from './../firebase/firebase';
import './admin.scss';

class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Notes: [] };
      }
      componentDidMount = () => {
        let localNotes = JSON.parse(localStorage.getItem("notes"));
        if (localNotes) {
          this.setState({
            Notes: localNotes
          });
        }
      };
      handleDeleteNotes = note => {
        let noteId = note.id;
        let newNotes = this.state.Notes.filter(function(note) {
          return note.id !== noteId;
        });
        this.setState({ Notes: newNotes });
      };
      componentDidUpdate = () => {
        this.updateStorage();
      };
      updateStorage = () => {
        let notes = JSON.stringify(this.state.Notes);
        localStorage.setItem("notes", notes);
      };
      handleAddNote = newNote => {
        console.info('handleAddNote called');
        this.firebaseRef = firebase.database().ref().child('notes')
        let newNotes = this.state.Notes.slice();
        newNotes.unshift(newNote);
        this.firebaseRef.push(newNote);
          this.setState({
            Notes: newNotes
          });
      };
    render(){
        return(
            <div>
                <div className='container admins'>
                    <div className='row'>
                        <div className='col-xl-6'>
                             {/* <Addico /> */}
                             <h4>Add ico</h4><br />
                             <TextArea noteAdd={this.handleAddNote} />
                                <hr />
                                  <NotesList
                                  Notes={this.state.Notes}
                                  handleDeleteNotes={this.handleDeleteNotes}
                                  />
                        </div>
                        <div className='col-xl-6'>
                            <Addnews />
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Admin;