import React, { Component } from 'react';
import './api-crypto.scss';
import axios from 'axios';
var NumberFormat = require('react-number-format');

class ApiCrypto extends Component {

  constructor(props) {
    super(props);

    this.state = {
      cryptos: []
    };
  }

  componentDidMount() {
    axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,IOT,LTC,DASH,BCH,WAVES&tsyms=USD')
      .then(res => {
        const cryptos = res.data;
        console.log(cryptos);
        this.setState({cryptos: cryptos});
      })
  }

//   api-crypto

  render() {
    return (
      <div className="App"> 
        {Object.keys(this.state.cryptos).map((key) => (

          <div id="crypto-container">
            <span className="left">{key}</span>
            <span className="right"><NumberFormat value={this.state.cryptos[key].USD} displayType={'text'} decimalPrecision={2} thousandSeparator={true} prefix={'$'} /></span>
          </div>

        ))}
      </div>
    );
  }
}

export default ApiCrypto;