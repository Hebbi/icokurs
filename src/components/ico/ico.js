import React from 'react';
import axios from 'axios';
import './ico.scss';
import NotesList from './../admin/noteslist/noteslist';
import ApiCrypto from './../api-crypto/api-crypto';

class Ico extends React.Component {
    constructor(props) {
        super(props);
        this.state = { Notes: [] };
      }
      componentDidMount = () => {
        let localNotes = JSON.parse(localStorage.getItem("notes"));
        if (localNotes) {
          this.setState({
            Notes: localNotes
          });
        }
      };
      handleDeleteNotes = note => {
        let noteId = note.id;
        let newNotes = this.state.Notes.filter(function(note) {
          return note.id !== noteId;
        });
        this.setState({ Notes: newNotes });
      };
      componentDidUpdate = () => {
        this.updateStorage();
      };
      updateStorage = () => {
        let notes = JSON.stringify(this.state.Notes);
        localStorage.setItem("notes", notes);
      };
    render(){
        return(
        <div>
            <div className='container-fluid'>
                <div className='row'>
                    <h3 className="h3-ico">ICO Projects</h3>
                </div>
            </div>
            <div className='container'>
                <div className="row">
                    <div className='col-xl-9'>
                        <NotesList
                        Notes={this.state.Notes}
                        handleDeleteNotes={this.handleDeleteNotes}
                        />
                    </div>
                    <div className='col-xl-3'>
                        <ApiCrypto />
                    </div>
                </div>
            </div>
        </div>

        );
    }
}

export default Ico;