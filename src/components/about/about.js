import React from 'react';
import './about.scss';

class About extends React.Component {
    render(){
        return(
            <div>
                <div className='container'>
                    <div className='row'>
                        <div className='col-xl-6'>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </p>
                            
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </p>
                        </div>
                        <div className='col-xl-6'>
                            <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </p>
                            <a href='tel:+380686484714'>+380686484714</a><br />
                            <a href='mail:alex.mayour2@gmail.com'>alex.mayour2@gmail.com</a>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default About;