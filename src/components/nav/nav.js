import React from 'react';
import { NavLink } from 'react-router-dom';
import './nav.scss';

class Nav extends React.Component {
    render(){
        return(
            <div>
                <div className='container-fluid header'>
                    <div className='row justify-content-center  align-items-end'>
                        <div className='col-xl-6'>
                                <ul>
                                    <li>
                                        <NavLink to="/" activeClassName='active'>logo</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/ico" activeClassName='active'>ico</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/news" activeClassName='active'>News</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/services" activeClassName='active'>Services</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/about" activeClassName='active'>About</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/contact" activeClassName='active'>Contacts</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/admin" activeClassName='active'>admin</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/login" activeClassName='active'>login</NavLink>
                                    </li>
                                </ul>
                        </div>

                    </div>
                </div>
            </div>

        );
    }
}

export default Nav;