import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import Router from './router';
import './index.css';
import './bootstrap4/css/bootstrap.min.css';
import Footer from './components/footer/footer';
// import Header from './comtonents/header/header'
// import { render } from 'react-dom';

class App extends React.Component {

    render(){
        return(
            <div>
                {/* <Header /> */}
                <BrowserRouter>

                <Router>
                </Router>

                </BrowserRouter>
                <Footer />
            </div>
        );
        
    }
}

ReactDOM.render(<App />, document.getElementById("root"));
