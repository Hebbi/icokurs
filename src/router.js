import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from './components/home/home'
import About from './components/about/about';
import Contact from './components/contacts/contact';
import Ico from './components/ico/ico';
import Nav from './components/nav/nav';
import NotFound from './components/404/404';
import News from './components/news/news';
import Admin from './components/admin/admin';
import Services from './components/services/services';
import Login from './components/login/login';
import './index.css';

class Router extends React.Component {
    render(){
        return(
            <div>
                <Nav />
                <Switch>
                    {/* підключаємо сторінки */}
                    <Route path="/" exact component={Home} />             
                    <Route path="/about" exact component={About} />
                    <Route path="/ico" exact component={Ico} />
                    <Route path="/news" exact component={News} />
                    <Route path="/contact" exact component={Contact} />
                    <Route path='/services' exact component={Services} />
                    <Route path='/admin' exact component={Admin} />
                    <Route path='/login' exact component={Login} />
                    <Route component={NotFound} />
                </Switch>
            </div>
        );
    }
}
export default Router;